import { Meteor } from 'meteor/meteor';
import {ConflictsCollection} from '../api/Conflicts.js';
import {PreventionsCollection} from '../api/Preventions.js';
import {RegionsCollection} from '../api/Regions.js';

Meteor.startup(() => {
  if(Meteor.isServer) {

    if(!Meteor.users.find({username: 'superadmin'}).count()) {
      Accounts.createUser({
        username: 'superadmin',
        email : 'superadmin@ank.kz',
        password :  'superadmin',
        profile: {
           roles: ['superadmin']
        }
      });
    }

    const regions = [
        {_id: '1', name: {ru: "Западно-Казахстанская область", kz: "Батыс Қазақстан облысы"}},
        {_id: '2', name: {ru: "Атырауская область", kz: "Атырау облысы"}},
        {_id: '3', name: {ru: "Мангистауская область", kz: "Маңғыстау облысы"}},
        {_id: '4', name: {ru: "Актюбинская область", kz: "Ақтөбе облысы"}},
        {_id: '5', name: {ru: "Костанайская область", kz: "Қостанай облысы"}},
        {_id: '6', name: {ru: "Кызылординская область", kz: "Қызылорда облысы"}},
        {_id: '7', name: {ru: "Северо-Казахстанская область", kz: "Солтүстік Қазақстан облысы"}},
        {_id: '8', name: {ru: "Акмолинская область", kz: "Ақмола облысы"}},
        {_id: '9', name: {ru: "Астана", kz: "Астана"}},
        {_id: '10', name: {ru: "Карагандинская область", kz: "Қарағанды облысы"}},
        {_id: '11', name: {ru: "Южно-Казахстанская область", kz: "Оңтүстік Қазақстан облысы"}},
        {_id: '12', name: {ru: "Жамбылская область", kz: "Жамбыл облысы"}},
        {_id: '13', name: {ru: "Павлодарская область", kz: "Павлодар облысы"}},
        {_id: '14', name: {ru: "Восточно-Казахстанская область", kz: "Шығыс Қазақстан облысы"}},
        {_id: '15', name: {ru: "Алматинская область", kz: "Алматы облысы"}},
        {_id: '16', name: {ru: "Алматы", kz: "Алматы"}}
      ];

      regions.forEach((region) => {
        if(RegionsCollection.find({_id: region._id}).count() === 0)
        {
          RegionsCollection.insert(region);
        }
      });

      const conflicts = [
        {_id: '1', name: "Социально-трудовые"},
        {_id: '2', name: "Социально-бытовые"},
        {_id: '3', name: "Гражданские"},
        {_id: '4', name: "Гражданско-правовые"},
      ];

      conflicts.forEach((conflict) => {
        if(ConflictsCollection.find({_id: conflict._id}).count() === 0)
        {
          ConflictsCollection.insert(conflict);
        }
      });

      const preventions = [
        {_id: '1', name: "Профессиональными медиаторами"},
        {_id: '2', name: "Непрофессиональными медиаторами"},
        {_id: '3', name: "Разрешение споров на стадии досудебного разбирательства"},
        {_id: '4', name: "Взаимодействие с государственными органами"},
      ];

      preventions.forEach((prevention) => {
        if(PreventionsCollection.find({_id: prevention._id}).count() === 0)
        {
          PreventionsCollection.insert(prevention);
        }
      });
    }
});
