export const CollaboratorsCollection = Meteor.users;

if(Meteor.isServer) {
  Meteor.publish('Collaborators', () => {
    return CollaboratorsCollection.find({username: {$ne: "superadmin"}}, {sort: {createdAt: -1}, limit: 20});
  })

  Meteor.methods({
    'collaborators.add'(data) {
      if(!Meteor.user().profile.roles.includes('superadmin'))
        return;

      Accounts.createUser({
        username: data.login,
        password :  data.password,
        profile: {
          fullName: data.fullName,
          contacts: data.contacts,
          iin: data.iin,
          phone: data.phone,
          roles: ['moderator'],
          region: data.region
        }
      });
    },
    'collaborators.remove'(_id) {
      CollaboratorsCollection.remove({_id});
    },
    'collaborators.edit'(_id, newUsername, newPass) {
      if(newPass !== "password")
        Accounts.setPassword(_id, newPass);
      Accounts.setUsername(_id, newUsername);
    }
  });
}
