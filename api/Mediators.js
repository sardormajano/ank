import {Mongo} from 'meteor/mongo';

export const MediatorsCollection = new Mongo.Collection('mediators');

if(Meteor.isServer) {
  Meteor.publish('Mediators', () => {
    return MediatorsCollection.find({}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.methods({
    'mediators.add'(data) {
      data.createdBy = Meteor.userId;
      const d = new Date();
      data.createdAt = Date.parse(d);

      MediatorsCollection.insert(data);
    },
    'mediators.remove'(_id) {
      MediatorsCollection.remove({_id})
    },
    'mediators.edit'(_id, data) {
      MediatorsCollection.update({_id}, {$set: data});
    }
  });
}
