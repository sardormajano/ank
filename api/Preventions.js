import {Mongo} from 'meteor/mongo';

export const PreventionsCollection = new Mongo.Collection('preventions');

if(Meteor.isServer) {
  Meteor.publish('Preventions', () => {
    return PreventionsCollection.find({}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.methods({
    'preventions.add'(data) {
      data.createdBy = Meteor.userId;
      const d = new Date();
      data.createdAt = Date.parse(d);

      PreventionsCollection.insert(data);
    },
    'preventions.remove'(_id) {
      PreventionsCollection.remove({_id})
    },
    'preventions.edit'(_id, data) {
      PreventionsCollection.update({_id}, {$set: data});
    }
  });
}
