import {Mongo} from 'meteor/mongo';

export const ReportsCollection = new Mongo.Collection('reports');

if(Meteor.isServer) {
  Meteor.publish('Reports', () => {
    return ReportsCollection.find({}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.methods({
    'reports.add'(data) {
      data.createdBy = Meteor.userId;
      const d = new Date();
      data.createdAt = Date.parse(d);

      ReportsCollection.insert(data);
    },
    'reports.remove'(_id) {
      ReportsCollection.remove({_id})
    },
    'reports.edit'(_id, data) {
      ReportsCollection.update({_id}, {$set: data});
    }
  });
}
