import {Mongo} from 'meteor/mongo';

export const ConflictsCollection = new Mongo.Collection('conflicts');

if(Meteor.isServer) {
  Meteor.publish('Conflicts', () => {
    return ConflictsCollection.find({}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.methods({
    'conflicts.add'(data) {
      data.createdBy = Meteor.userId;
      const d = new Date();
      data.createdAt = Date.parse(d);

      ConflictsCollection.insert(data);
    },
    'conflicts.remove'(_id) {
      ConflictsCollection.remove({_id})
    },
    'conflicts.edit'(_id, data) {
      ConflictsCollection.update({_id}, {$set: data});
    }
  });
}
