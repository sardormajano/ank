module.exports = {
  servers: {
    one: {
      host: '138.197.90.60',
      username: 'root',
      // pem:
      password: '4thebest'
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'ANK',
    path: '.',
    // docker: {
    //   image: 'kadirahq/meteord'
    // },
    servers: {
      one: {}
    },
    // buildOptions: {
    //   serverOnly: true
    // },
    env: {
      ROOT_URL: 'http://138.197.90.60',
      MONGO_URL: 'mongodb://localhost/meteor'
    },

    dockerImage: 'abernix/meteord:base',
    deployCheckWaitTime: 120
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};
