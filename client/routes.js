import React from 'react';

import App from './components/App.jsx';

import {Router, Route, IndexRoute, browserHistory} from 'react-router';

import MainPageContainer from './containers/MainPageContainer.jsx';
import DictionariesContainer from './containers/DictionariesContainer.jsx';
import ReportsContainer from './containers/ReportsContainer.jsx';
import MapsContainer from './containers/MapsContainer.jsx';
import LoginContainer from './containers/LoginContainer.jsx';
import SingleRegionContainer from './containers/SingleRegionContainer.jsx';

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={MainPageContainer} />
      <Route path="dictionaries" component={DictionariesContainer} />
      <Route path="reports" component={ReportsContainer} />
      <Route path="maps" component={MapsContainer} />
      <Route path="single-region" component={SingleRegionContainer} />
      <Route path="login" component={LoginContainer} />
    </Route>
  </Router>
);
