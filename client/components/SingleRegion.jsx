import React, {Component} from 'react';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import Snackbar from 'material-ui/Snackbar';

import TabGeneral from './stateless/SingleRegionTabGeneral.jsx';
import TabMediators from './stateless/SingleRegionTabMediators.jsx';
import TabConflicts from './stateless/SingleRegionTabConflicts.jsx';

import getMuiTheme from 'material-ui/styles/getMuiTheme';

import HeaderContainer from '../containers/HeaderContainer';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  tabLabel: {
    color: 'white',
    fontWeight: '200'
  },
  snackbar: {
    backgroundColor: 'darkBlue',
    color: 'white'
  },
  cards: {
    margin: '1em'
  }
}

export default class SingleRegion extends Component {
  render() {
    const {snackbar, currentReports, currentRegion} = this.props;

    return (
      <div>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <HeaderContainer title={`${currentRegion ? currentRegion.name.ru : ''} - Информация о регионе`}/>
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Tabs>
            <Tab
              style={styles.tabLabel}
              label="Общая информация">
              <TabGeneral
                styles={styles}
                currentReports={currentReports}
                currentRegion={currentRegion}
              />
            </Tab>
            <Tab
              style={styles.tabLabel}
              label="Медиаторы">
              <TabMediators
                styles={styles}
                currentReports={currentReports}
              />
            </Tab>
            <Tab
              style={styles.tabLabel}
              label="Конфликты и споры">
              <TabConflicts
                styles={styles}
                currentReports={currentReports}
              />
            </Tab>
          </Tabs>
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Snackbar
            open={snackbar.open}
            message={snackbar.message}
            autoHideDuration={4000}
            onRequestClose={snackbar.closeHandler}
            style={styles.snackbar}
          />
        </MuiThemeProvider>
      </div>
    );
  }
}
