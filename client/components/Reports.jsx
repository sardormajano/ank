import React, {Component} from 'react';

import HeaderContainer from '../containers/HeaderContainer.jsx';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Chip from 'material-ui/Chip';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  tabLabel: {
    color: 'white',
    fontWeight: '200'
  },
  snackbar: {
    backgroundColor: 'darkBlue',
    color: 'white'
  },
  cards: {
    margin: '1em'
  },
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
}

export default class Reports extends Component {
  render() {
    const {
      touchTapHandler, changeHandler, chipDeleteHandler, selectChangeHandler,
      clearFormHandler, values, snackbar, context, conflicts, addReportHandler,
      reportRows
    } = this.props,
          conflictOptions = conflicts.map((item) => {
            return (
              <MenuItem
                key={item._id}
                value={item._id}
                primaryText={item.name}/>
            );
          }),
          conflictChips = values.conflictsArray.map((item) => {
            return (
              <Chip
                key={item._id}
                style={styles.chip}
                onRequestDelete={chipDeleteHandler.bind(context, item._id)}>
                {item.name}
              </Chip>
            );
          });

    return (
      <div>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <HeaderContainer title="Отчеты"/>
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Card style={styles.cards}>
            <CardHeader
              title="Добавление отчета"
              subtitle="(деятельность по медиации)"
            />
            <CardText>
              <TextField
                hintText="Наличие кабинетов мадиации в Домах дружбы"
                data-state-name="cabinetsNumber"
                onChange={changeHandler}
                fullWidth={true}
                value={values.cabinetsNumber}
              />
              <TextField
                hintText="Наличие телефона доверия"
                data-state-name="phone"
                onChange={changeHandler}
                fullWidth={true}
                value={values.phone}
              />
              <SelectField
                floatingLabelText="Советы медиации АНК"
                value={values.mediationBoard}
                onChange={selectChangeHandler.bind(context, "mediationBoard")}
                fullWidth={true}
              >
                <MenuItem
                  value={true}
                  primaryText='Создан'/>
                <MenuItem
                  value={false}
                  primaryText='Не создан'/>
              </SelectField>
              <TextField
                hintText="Практикующие профессиональные медиаторы"
                data-state-name="professionalMediatorsNumber"
                onChange={changeHandler}
                fullWidth={true}
                value={values.professionalMediatorsNumber}
              />
              <TextField
                hintText="Непрофессиональные (общественные) медиаторы"
                data-state-name="nonProfessionalMediatorsNumber"
                onChange={changeHandler}
                fullWidth={true}
                value={values.nonProfessionalMediatorsNumber}
              />
              <TextField
                hintText="Разрешено споров и конфликтов"
                data-state-name="disputesResolved"
                onChange={changeHandler}
                fullWidth={true}
                value={values.disputesResolved}
              />
              <br/><br/>
              <div style={styles.wrapper}>
                {conflictChips}
              </div>
              <SelectField
                floatingLabelText="Виды разрешенных спорных ситуаций"
                value={values.disputesResolvedTypes}
                onChange={selectChangeHandler.bind(context, "disputesResolvedTypes")}
                fullWidth={true}
              >
                {conflictOptions}
              </SelectField>
              <TextField
                hintText="Проведено заседаний/совещаний по вопросам медиации"
                data-state-name="meetingsNumber"
                onChange={changeHandler}
                fullWidth={true}
                value={values.meetingsNumber}
              />
              <Card style={styles.cards}>
                <CardHeader
                  title="Превентивные меры"
                />
                <CardText>
                  <TextField
                    hintText="Профессиональными медиаторами"
                    data-state-name="byProfessionalMediatorsNum"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.byProfessionalMediatorsNum}
                  />
                  <TextField
                    hintText="Непрофессиональными медиаторами"
                    data-state-name="byNonProfessionalMediatorsNum"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.byNonProfessionalMediatorsNum}
                  />
                  <TextField
                    hintText="Разрешение споров на стадии судебного разбирательства"
                    data-state-name="byTrialsNum"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.byTrialsNum}
                  />
                  <TextField
                    hintText="Взаимодействие с акиматами, государственными органами и институтами гражданского общества"
                    data-state-name="byGovernmentBodiesNum"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.byGovernmentBodiesNum}
                  />
                  <TextField
                    hintText="Количество открытых кабинетов медиации на День медиации"
                    data-state-name="currentCabinetsNum"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.currentCabinetsNum}
                  />
                </CardText>
              </Card>
            </CardText>
            <CardActions>
              <FlatButton label="Добавить" onClick={addReportHandler}/>
              <FlatButton label="Очистить форму" onClick={clearFormHandler}/>
            </CardActions>
          </Card>
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Card style={styles.cards}>
            <CardHeader
              title="Таблица видов превентивных мер"
            />
            <CardText>
              <Table>
                <TableHeader>
                  <TableRow>
                    <TableHeaderColumn>ID</TableHeaderColumn>
                    <TableHeaderColumn>Дата отчета</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {reportRows}
                </TableBody>
              </Table>
            </CardText>
          </Card>
        </MuiThemeProvider>
      </div>
    );
    ;
  }
}
