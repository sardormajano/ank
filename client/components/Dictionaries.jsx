import React, {Component} from 'react';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import HeaderContainer from '../containers/HeaderContainer';

import AppBar from 'material-ui/AppBar';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  tabLabel: {
    color: 'white',
    fontWeight: '200'
  },
  snackbar: {
    backgroundColor: 'darkBlue',
    color: 'white'
  },
  cards: {
    margin: '1em'
  }
}

export default class Dictionaries extends Component {
  render() {
    const {
      touchTapHandler, changeHandler, selectChangeHandler, regionChangeHandler, regions,
      addCollaboratorHandler, addMediatorHandler, addConflictHandler, addPreventionHandler,
      clearFormHandler, values, snackbar,
      collaboratorRows, mediatorRows, conflictRows, preventionRows, context, hideCollaboratorsTab
    } = this.props,

        regionOptions = regions.map((regionItem) => {
          return (
            <MenuItem
              key={regionItem.name.ru}
              value={regionItem._id}
              primaryText={regionItem.name.ru}/>
          );
        });

    let collaboratorTabJSX;

    if(hideCollaboratorsTab) {
      collaboratorTabJSX = [];
    }
    else {
      collaboratorTabJSX = (
        <Tab
          style={styles.tabLabel}
          label="Сотрудники" >

          <Card style={styles.cards}>
            <CardHeader
              title="Добавление сотрудника"
              subtitle="(модератора)"
            />
            <CardText>
              <SelectField
                floatingLabelText="Регион"
                value={values.region}
                onChange={selectChangeHandler.bind(context, "region")}
                fullWidth={true}
              >
                {regionOptions}
              </SelectField>
              <TextField
                hintText="Логин"
                data-state-name="login"
                onChange={changeHandler}
                fullWidth={true}
                value={values.login}
              />
              <TextField
                hintText="Пароль"
                type="password"
                data-state-name="password"
                onChange={changeHandler}
                fullWidth={true}
                value={values.password}
              />
              <TextField
                hintText="ФИО"
                data-state-name="cfullName"
                onChange={changeHandler}
                fullWidth={true}
                value={values.cfullName}
              />
              <TextField
                hintText="ИИН"
                data-state-name="ciin"
                onChange={changeHandler}
                fullWidth={true}
                value={values.ciin}
              />
              <TextField
                hintText="Номер телефона"
                data-state-name="cphone"
                onChange={changeHandler}
                fullWidth={true}
                value={values.cphone}
              />
              <TextField
                hintText="Контактная информация"
                data-state-name="ccontacts"
                onChange={changeHandler}
                fullWidth={true}
                value={values.ccontacts}
              />
            </CardText>
            <CardActions>
              <FlatButton label="Добавить" onClick={addCollaboratorHandler}/>
              <FlatButton label="Очистить форму" onClick={clearFormHandler}/>
            </CardActions>
          </Card>
          <Snackbar
            open={snackbar.open}
            message={snackbar.message}
            autoHideDuration={4000}
            onRequestClose={snackbar.closeHandler}
            style={styles.snackbar}
          />

          <Card style={styles.cards}>
            <CardHeader
              title="Таблица сотрудников"
            />
            <CardText>
              <Table>
                <TableHeader>
                  <TableRow>
                    <TableHeaderColumn>ID</TableHeaderColumn>
                    <TableHeaderColumn>Логин</TableHeaderColumn>
                    <TableHeaderColumn>ФИО</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {collaboratorRows}
                </TableBody>
              </Table>
            </CardText>
          </Card>
        </Tab>
      );
    }

    return (
      <div>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <HeaderContainer title="Словари"/>
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Tabs>
            {collaboratorTabJSX}
            <Tab
              style={styles.tabLabel}
              label="Практикующие медиаторы" >

              <Card style={styles.cards}>
                <CardHeader
                  title="Добавление практикующего медиатора"
                  subtitle="(профессионального или непрофессионального)"
                />
                <CardText>
                  <TextField
                    hintText="ФИО"
                    data-state-name="fullName"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.fullName}
                  />
                  <TextField
                    hintText="ИИН"
                    data-state-name="iin"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.iin}
                  />
                  <TextField
                    hintText="Номер телефона"
                    data-state-name="phone"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.phone}
                  />
                  <TextField
                    hintText="Контактная информация"
                    data-state-name="contacts"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.contacts}
                  />
                </CardText>
                <CardActions>
                  <FlatButton label="Добавить" onClick={addMediatorHandler}/>
                  <FlatButton label="Очистить форму" onClick={clearFormHandler}/>
                </CardActions>
              </Card>
              <Snackbar
                open={snackbar.open}
                message={snackbar.message}
                autoHideDuration={4000}
                onRequestClose={snackbar.closeHandler}
                style={styles.snackbar}
              />

              <Card style={styles.cards}>
                <CardHeader
                  title="Таблица практикующих медиаторов"
                />
                <CardText>
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHeaderColumn>ID</TableHeaderColumn>
                        <TableHeaderColumn>ФИО</TableHeaderColumn>
                        <TableHeaderColumn>ИИН</TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                      {mediatorRows}
                    </TableBody>
                  </Table>
                </CardText>
              </Card>
            </Tab>
            <Tab
              style={styles.tabLabel}
              label="Виды конфликтов" >

              <Card style={styles.cards}>
                <CardHeader
                  title="Добавление вида конфликтов"
                />
                <CardText>
                  <TextField
                    hintText="Наименование"
                    data-state-name="conflictName"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.conflictName}
                  />
                </CardText>
                <CardActions>
                  <FlatButton label="Добавить" onClick={addConflictHandler}/>
                  <FlatButton label="Очистить форму" onClick={clearFormHandler}/>
                </CardActions>
              </Card>

              <Card style={styles.cards}>
                <CardHeader
                  title="Таблица видов конфликтов"
                />
                <CardText>
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHeaderColumn>ID</TableHeaderColumn>
                        <TableHeaderColumn>Наименование</TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                      {conflictRows}
                    </TableBody>
                  </Table>
                </CardText>
              </Card>
            </Tab>
            <Tab
              style={styles.tabLabel}
              label="Виды превентивных мер" >

              <Card style={styles.cards}>
                <CardHeader
                  title="Добавление превентивной меры"
                />
                <CardText>
                  <TextField
                    hintText="Наименование"
                    data-state-name="preventionName"
                    onChange={changeHandler}
                    fullWidth={true}
                    value={values.preventionName}
                  />
                </CardText>
                <CardActions>
                  <FlatButton label="Добавить" onClick={addPreventionHandler}/>
                  <FlatButton label="Очистить форму" onClick={clearFormHandler}/>
                </CardActions>
              </Card>

              <Card style={styles.cards}>
                <CardHeader
                  title="Таблица видов превентивных мер"
                />
                <CardText>
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHeaderColumn>ID</TableHeaderColumn>
                        <TableHeaderColumn>Наименование</TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                      {preventionRows}
                    </TableBody>
                  </Table>
                </CardText>
              </Card>
            </Tab>
          </Tabs>
        </MuiThemeProvider>
      </div>
    );
  }
}
