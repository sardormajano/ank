import React, {Component} from 'react';

import HeaderContainer from '../containers/HeaderContainer.jsx';

export default class MainPage extends Component {
  render() {
    return <HeaderContainer title="Главная"/>;
  }
}
