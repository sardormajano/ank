import React, {Component} from 'react';

import HeaderContainer from '../containers/HeaderContainer.jsx';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import ReactSVG from 'react-svg';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import getMuiTheme from 'material-ui/styles/getMuiTheme';

const styles = {
  media: {
    padding: '0 16em'
  },
  cards: {
    margin: '1em'
  },
  dialog: {
    marginTop: '-5em'
  }
};

export default class Maps extends Component {
  render() {
    const {onload, changeId, dialog} = this.props;

    return (
      <div>
        <HeaderContainer title="Карты"/>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Card style={styles.cards}>
            <CardHeader title="Карта регионов РК" />
            <CardMedia style={styles.media}>
              <ReactSVG
                path="/svg/kazakhstan.svg"
                className="svg-map"
                callback={onload}/>
            </CardMedia>
            <CardActions>
              <FlatButton label="Открыть регион" />
            </CardActions>
            <Dialog
              title="Dialog With Actions"
              actions={dialog.actions}
              contentStyle={styles.dialog}
              style={styles.dialog}
              open={dialog.open}
              onRequestClose={dialog.handleClose}
              autoScrollBodyContent={true}
            >
              <ReactSVG
                path={"/svg/"+dialog.mapName+".svg"}
                className="svg-map"/>
            </Dialog>
          </Card>
        </MuiThemeProvider>
      </div>
    );
  }
}
