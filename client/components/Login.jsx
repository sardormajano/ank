import React, {Component} from 'react';

import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';

import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import getMuiTheme from 'material-ui/styles/getMuiTheme';

import TextField from 'material-ui/TextField';

const styles = {
  flexContainer: {
    height: '100%',
    padding: '100px 0',
    margin: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexItem: {
    width: '50%',
    padding: '3em'
  },
  snackbar: {
    success: {
      backgroundColor: 'darkBlue',
      color: 'white',
      textAlign: 'center'
    },
    fail: {
      backgroundColor: 'red',
      color: 'white',
      textAlign: 'center'
    }
  }
}

export default class Login extends Component {
  render() {
    const {
      loginHandler, login, password, changeHandler,
      context, snackbar
    } = this.props;

    return (
      <div style={styles.flexContainer}>
        <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
          <Card style={styles.flexItem}>
            <CardHeader
              title="Авторизация"
              subtitle="Введите логин и пароль"
            />
            <CardText>
              <TextField
                hintText="Ваш логин"
                value={login}
                fullWidth={true}
                data-state-name='login'
                onChange={changeHandler}
              />
              <TextField
                hintText="Ваш пароль"
                value={password}
                type='password'
                data-state-name='password'
                onChange={changeHandler}
                fullWidth={true}
              />
            </CardText>
            <CardActions>
              <FlatButton label="Войти" onClick={loginHandler}/>
            </CardActions>
            <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
              <Snackbar
                style={{backgroundColor: 'red'}}
                open={snackbar.open}
                message={snackbar.message}
                autoHideDuration={4000}
                onRequestClose={snackbar.closeHandler}
                contentStyle={styles.snackbar[snackbar.snackbarType]}
              />
            </MuiThemeProvider>
          </Card>
        </MuiThemeProvider>
      </div>
    );
  }
}
