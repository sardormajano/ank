import React, {Component} from 'react';

import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import getMuiTheme from 'material-ui/styles/getMuiTheme';

import {Link} from 'react-router';

const styles = {
  tabLabel: {
    color: 'white',
    fontWeight: '200'
  },
  link: {
    color: 'white',
    fontWeight: '200',
    textDecoration: 'none'
  }
};

export default class Header extends Component {
  render() {
    const {
      menuItemHandler, handleToggle, handleClose,
      handleOpen, open, title, logoutHandler
    } = this.props;

    return (
      <div>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <AppBar
            title={title}
            iconClassNameRight="muidocs-icon-navigation-expand-more"
            onLeftIconButtonTouchTap={handleToggle}
          />
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <Drawer
            docked={false}
            width={200}
            open={open}
            onRequestChange={handleOpen}
          >
            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
              <AppBar title="Меню" onLeftIconButtonTouchTap={handleToggle}/>
            </MuiThemeProvider>
            <MenuItem onClick={menuItemHandler} data-href="dictionaries">Словари</MenuItem>
            <MenuItem onClick={menuItemHandler} data-href="reports">Отчет</MenuItem>
            <MenuItem onClick={menuItemHandler} data-href="maps">Карты</MenuItem>
            <br/><br/><br/>
            <MenuItem onClick={logoutHandler} data-href="maps">Выйти</MenuItem>
          </Drawer>
        </MuiThemeProvider>
      </div>
    );
  }
}
