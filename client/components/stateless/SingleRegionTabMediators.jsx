import React, {Component} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {GridList, GridTile} from 'material-ui/GridList';

import Chart from 'chart.js';

const styles = {
  root: {
    margin: '1em',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '100%',
    overflowY: 'auto',
  },
  cards: {
    margin: '1em'
  }
}

export default class SingleRegionTabMediator extends Component {
  componentDidMount() {
    const myChart = new Chart(this.ctx1, {
      type: 'doughnut',
      data: {
        labels: ["Профессиональные медиаторы", "Непрофессиональные медиаторы"],
        datasets: [{
            label: 'Количество проф. и непроф. медиаторов за последний месяц',
            data: [12, 23],
            backgroundColor: [
                '#FF6384',
                '#36A2EB',
            ],
            borderColor: [
                'white',
                'white',
            ],
            borderWidth: 5
        }]
      }
    });
    const myChart2 = new Chart(this.ctx2, {
      type: 'line',
      data: {
        labels: [
          "Август 2016", "Сентябрь 2016", "Октябрь 2016", "Ноябрь 2016",
           "Декабрь 2016", "Январь 2017", "Февраль 2017"
        ],
        datasets: [
          {
            label: 'Рост количества профессиональных медиаторов',
            data: [33, 33, 36, 40, 45, 42, 50,],
            borderColor: '#6ccbcb',
            pointBackgroundColor: [
              '#FF6384', '#36A2EB', '#FF6384', '#36A2EB',
              '#FF6384', '#36A2EB', '#FF6384'
            ],
            fill: false,
          },
          {
            label: 'Рост количества непрофессиональных медиаторов',
            data: [40, 45, 42, 50, 33, 33, 36, ],
            borderColor: '#e8564f',
            pointBackgroundColor: [
              '#FF6384', '#36A2EB', '#FF6384', '#36A2EB',
              '#FF6384', '#36A2EB', '#FF6384'
            ],
            fill: false,
          },
      ]
      }
    });
  }

  render() {
    const {styles, currentReports} = this.props;

    return (
      <Card style={styles.cards}>
        <CardHeader title="Информация о медиаторах" />
        <CardText style={styles.root}>
          <GridList
            cols={2}
            cellHeight={700}
            style={styles.gridList}
          >
            <GridTile>
              <canvas ref={(ctx1) => {this.ctx1 = ctx1;}} height={300}/>
            </GridTile>
            <GridTile>
              <canvas ref={(ctx2) => {this.ctx2 = ctx2;}} height={300}/>
            </GridTile>
          </GridList>
        </CardText>
      </Card>
    );
  }
}
