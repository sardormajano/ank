import React, {Component} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {GridList, GridTile} from 'material-ui/GridList';

import Chart from 'chart.js';

const styles = {
  root: {
    margin: '1em',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '100%',
    overflowY: 'auto',
  },
  cards: {
    margin: '1em'
  }
}

export default class SingleRegionTabConflicts extends Component {
  componentDidMount() {
    const myChart = new Chart(this.ctx1, {
      type: 'pie',
      data: {
        labels: [
          "Социально-бытовые", "Социально-трудовые",
          "Гражданские", "Гражданско-правовые",
          "Социально-трудовые"
        ],
        datasets: [{
            label: 'Количество проф. и непроф. медиаторов за последний месяц',
            data: [65, 23, 84, 33, 54],
            backgroundColor: [
              'rgba(255, 99, 132, 0.8)',
              'rgba(54, 162, 235, 0.8)',
              'rgba(255, 206, 86, 0.8)',
              'rgba(75, 192, 192, 0.8)',
              'rgba(153, 102, 255, 0.8)'
            ],
            borderColor: [
                'black',
                'black',
                'black',
                'black',
                'black',
            ],
            borderWidth: 2
        }]
      }
    });
    const myChart2 = new Chart(this.ctx2, {
      type: 'bar',
      data: {
        labels: [
          "Социально-бытовые", "Социально-трудовые",
          "Гражданские", "Гражданско-правовые",
          "Социально-трудовые"
        ],
        datasets: [
          {
            label: "Сравнение количества споров/конфликтов",
            data: [65, 23, 84, 33, 54],
            borderColor: '#6ccbcb',
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
            ],
            borderWidth: 3,
          }
      ]
      }
    });
  }

  render() {
    const {styles, currentReports} = this.props;

    return (
      <Card style={styles.cards}>
        <CardHeader title="Информация о конфликтах и спорах" />
        <CardText style={styles.root}>
          <GridList
            cols={2}
            cellHeight={700}
            style={styles.gridList}
          >
            <GridTile>
              <canvas ref={(ctx2) => {this.ctx2 = ctx2;}} height={300}/>
            </GridTile>
            <GridTile>
              <canvas ref={(ctx1) => {this.ctx1 = ctx1;}} height={300}/>
            </GridTile>
          </GridList>
        </CardText>
      </Card>
    );
  }
}
