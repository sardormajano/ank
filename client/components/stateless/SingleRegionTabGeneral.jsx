import React, {Component} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import CommunicationCall from 'material-ui/svg-icons/communication/call';
import AccountBalance from 'material-ui/svg-icons/action/account-balance';
import AccountBox from 'material-ui/svg-icons/action/account-box';
import PeopleOutline from 'material-ui/svg-icons/social/people-outline';
import Business from 'material-ui/svg-icons/communication/business';
import {indigo500} from 'material-ui/styles/colors';

export default class SingleRegionTabGeneral extends Component {
  render() {
    const {styles, currentReports, currentRegion} = this.props;

    return (
      <Card style={styles.cards}>
        <CardHeader
          title={`${currentRegion ? currentRegion.name.ru : ''} - Общая информация о регионе`}
          subtitle="последнее обновление 21.02.2017"
        />
        <CardText>
          <List>
            <ListItem
              leftIcon={<CommunicationCall color={indigo500} />}
              primaryText="(650) 555 - 1234"
              secondaryText="Телефон доверия"
            />
            <ListItem
              leftIcon={<AccountBalance color={indigo500} />}
              primaryText="8"
              secondaryText="Наличие кабинетов медиации в Домах дружбы"
            />
            <ListItem
              leftIcon={<Business color={indigo500} />}
              primaryText="7"
              secondaryText="Проведено заседаний/совещаний по вопросам медиации"
            />
            <ListItem
              leftIcon={<PeopleOutline color={indigo500} />}
              primaryText="не создан"
              secondaryText="Совет медиации"
            />
            <ListItem
              leftIcon={<AccountBox color={indigo500} />}
              primaryText="Картыбаев Бауржан"
              secondaryText="Автор последнего обновления"
            />
          </List>
        </CardText>
      </Card>
    );
  }
}
