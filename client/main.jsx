import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';
import {renderRoutes} from './routes.js';

Meteor.startup(function() {

  const root = document.createElement('div');
  root.setAttribute('id', 'root');
  document.body.appendChild(root);

  // Needed for onTouchTap
  import injectTapEventPlugin from 'react-tap-event-plugin';
  injectTapEventPlugin();
  render(renderRoutes(), root);
});
