import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import MainPage from '../components/MainPage.jsx';

class MainPageContainer extends Component {
  render() {
    return <MainPage />;
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
  };
}, MainPageContainer);
