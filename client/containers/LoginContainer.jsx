import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import Login from '../components/Login.jsx';

class LoginContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      snackbarOpen: false,
      snackbarMessage: '',
      snackbarType: ''
    };
  }

  loginHandler(e) {
    e.preventDefault();

    Meteor.loginWithPassword(this.state.login, this.state.password, (err) => {
      if(err) {
        this.setState({
          snackbarMessage: 'Неправильный логин или пароль!',
          snackbarOpen: true,
          snackbarType: 'fail'
        });
      }
      else {
        this.setState({
          snackbarMessage: 'Добро пожаловать!',
          snackbarOpen: true,
          preventionName: '',
          snackbarType: 'success'
        });

        setTimeout(() => {
          browserHistory.push('/');
        }, 2000);
      }
    });
  }

  changeHandler(e, newValue) {
    const stateName = e.currentTarget.getAttribute('data-state-name')

    this.setState({
      [stateName]: newValue
    });
  }

  closeHandler(e) {
    this.setState({
      snackbarOpen: false
    });
  }

  render() {
    const snackbar = {
      message: this.state.snackbarMessage,
      open: this.state.snackbarOpen,
      snackbarType: this.state.snackbarType,
      closeHandler: this.closeHandler.bind(this)
    };

    return <Login
      login={this.state.login}
      password={this.state.password}
      loginHandler={this.loginHandler.bind(this)}
      changeHandler={this.changeHandler.bind(this)}
      context={this}
      snackbar={snackbar}/>;
  }
}

export default createContainer(() => {
  if(Meteor.user())
  {
    setTimeout(() => {
      browserHistory.push('/');
    }, 2000);
  }

  return {
    user: Meteor.user(),
  };
}, LoginContainer);
