import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import Header from '../components/Header.jsx';

class HeaderContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  menuItemHandler(e) {
    e.preventDefault();
    const href = e.currentTarget.getAttribute('data-href');
    this.setState({open: !this.state.open});
    browserHistory.push(href);
  }

  logoutHandler(e) {
    e.preventDefault();

    Meteor.logout((err) => {
      if(err) {
        console.log(err);
      }
      else {
        console.log('До свидания');
      }
    });
  }

  handleToggle() {
    this.setState({open: !this.state.open});
  }

  handleClose() {
    this.setState({open: false});
  }

  handleOpen() {
    this.setState({open: true});
  }

  render() {
    return (
      <Header
        menuItemHandler={this.menuItemHandler.bind(this)}
        logoutHandler={this.logoutHandler.bind(this)}
        handleClose={this.handleClose.bind(this)}
        handleToggle={this.handleToggle.bind(this)}
        handleOpen={this.handleOpen.bind(this)}
        open={this.state.open}
        title={this.props.title}/>
    );
  }
}

export default createContainer(() => {
  if(Meteor.user() === null)
  {
    browserHistory.push('/login');
  }

  return {
    user: Meteor.user()
  };
}, HeaderContainer);
