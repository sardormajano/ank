import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import FlatButton from 'material-ui/FlatButton';

import Maps from '../components/Maps.jsx';

class MapsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: undefined,
      open: false,
      mapName: ''
    }
  }

  onload(mapdom) {
    const map = $(mapdom);

    mapDocument = map.contents(),
    mapics = mapDocument.filter("#maps").children();

    mapics.on('click', (e) => {
      const id = e.currentTarget.getAttribute('data-region-id');
      browserHistory.push(`single-region?id=${id}`);
    });
  }

  changeId(id) {
    try {
      this.setState({id});
    }
    catch(err) {
      this.setState({id});
    }
  }

  handleOpen() {
    this.setState({open: true});
  }

  handleClose() {
    this.setState({open: false});
  }

  render() {
    const actions = [
          <FlatButton
            label="Закрыть"
            primary={true}
            keyboardFocused={true}
            onTouchTap={this.handleClose.bind(this)}
          />,
        ],
        dialog = {
          actions,
          handleClose: this.handleClose.bind(this),
          handleOpen: this.handleOpen.bind(this),
          open: this.state.open,
          mapName: this.state.mapName
        };


    return <Maps
      onload={this.onload.bind(this)}
      actions={actions}
      dialog={dialog} />;
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
  };
}, MapsContainer);
