import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Dictionaries from '../components/Dictionaries.jsx';
import {TableRow, TableRowColumn} from 'material-ui/Table';

import {CollaboratorsCollection} from '../../api/Collaborators.js';
import {MediatorsCollection} from '../../api/Mediators.js';
import {ConflictsCollection} from '../../api/Conflicts.js';
import {PreventionsCollection} from '../../api/Preventions.js';
import {RegionsCollection} from '../../api/Regions.js';

class DictionariesContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fullName: '',
      iin: '',
      phone: '',
      contacts: '',
      login: '',
      password: '',
      region: '9',
      cfullName: '',
      ciin: '',
      cphone: '',
      ccontacts: '',
      conflictName: '',
      preventionName: '',
      snackbarOpen: false,
      snackbarMessage: ''
    };
  }

  componentWillMount() {
    Meteor.subscribe('Collaborators');
    Meteor.subscribe('Mediators');
    Meteor.subscribe('Conflicts');
    Meteor.subscribe('Preventions');
    Meteor.subscribe('Regions');
  }

  touchTapHandler(e) {
    e.preventDefault();
  }

  changeState(value, name) {
    this.setState({
      [name]: value
    });
  }

  changeHandler(e, newValue) {
    const stateName = e.currentTarget.getAttribute('data-state-name');

    this.setState({
      [stateName]: newValue
    });
  }

  selectChangeHandler(statename, e, index, payload) {
    const stateName = e.target.getAttribute('data-state-name');

    this.setState({
      [statename]: payload
    });
  }

  addCollaboratorHandler(e) {
    e.preventDefault();

    const data = {
      login: this.state.login,
      password: this.state.password,
      fullName: this.state.cfullName,
      iin: this.state.ciin,
      phone: this.state.cphone,
      contacts: this.state.ccontacts,
      region: this.state.region
    };

    Meteor.call('collaborators.add', data, (err) => {
      if(err) {
        this.setState({
          snackbarMessage: err.reason,
          snackbarOpen: true,
        });
      }
      else {
        this.setState({
          snackbarMessage: 'Сотрудник успешно добавлен!',
          snackbarOpen: true,
          login: '',
          password: '',
          cfullName: '',
          ciin: '',
          cphone: '',
          ccontacts: ''
        });
      }
    });
  }

  addMediatorHandler(e) {
    e.preventDefault();

    const data = {
      fullName: this.state.fullName,
      iin: this.state.iin,
      phone: this.state.phone,
      contacts: this.state.contacts
    };

    Meteor.call('mediators.add', data, (err) => {
      if(err) {
        this.setState({
          snackbarMessage: err.reason,
          snackbarOpen: true,
        });
      }
      else {
        this.setState({
          snackbarMessage: 'Медиатор успешно добавлен!',
          snackbarOpen: true,
          fullName: '',
          iin: '',
          phone: '',
          contacts: ''
        });
      }
    });
  }

  addConflictHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.conflictName
    };

    Meteor.call('conflicts.add', data, (err) => {
      if(err) {
        this.setState({
          snackbarMessage: err.reason,
          snackbarOpen: true,
        });
      }
      else {
        this.setState({
          snackbarMessage: 'Конфликт успешно добавлен!',
          snackbarOpen: true,
          conflictName: ''
        });
      }
    });
  }

  addPreventionHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.preventionName
    };

    Meteor.call('preventions.add', data, (err) => {
      if(err) {
        this.setState({
          snackbarMessage: err.reason,
          snackbarOpen: true,
        });
      }
      else {
        this.setState({
          snackbarMessage: 'Превентивная мера успешно добавлена!',
          snackbarOpen: true,
          preventionName: ''
        });
      }
    });
  }

  clearFormHandler(e) {
    e.preventDefault();

    this.setState({
      fullName: '',
      iin: '',
      phone: '',
      contacts: '',
      conflictName: '',
      preventionName: ''
    });
  }

  closeHandler(e) {
    this.setState({
      snackbarOpen: false
    });
  }

  getCollaboratorRows() {
    return this.props.collaborators.map((collaborator) => {
      return (
        <TableRow key={collaborator._id}>
          <TableRowColumn>{collaborator._id}</TableRowColumn>
          <TableRowColumn>{collaborator.username}</TableRowColumn>
          <TableRowColumn>{collaborator.profile.fullName}</TableRowColumn>
        </TableRow>
      );
    });
  }

  getMediatorRows() {
    return this.props.mediators.map((mediator) => {
      return (
        <TableRow key={mediator._id}>
          <TableRowColumn>{mediator._id}</TableRowColumn>
          <TableRowColumn>{mediator.fullName}</TableRowColumn>
          <TableRowColumn>{mediator.iin}</TableRowColumn>
        </TableRow>
      );
    });
  }

  getConflictRows() {
    return this.props.conflicts.map((conflict) => {
      return (
        <TableRow key={conflict._id}>
          <TableRowColumn>{conflict._id}</TableRowColumn>
          <TableRowColumn>{conflict.name}</TableRowColumn>
        </TableRow>
      );
    });
  }

  getPreventionRows() {
    return this.props.preventions.map((prevention) => {
      return (
        <TableRow key={prevention._id}>
          <TableRowColumn>{prevention._id}</TableRowColumn>
          <TableRowColumn>{prevention.name}</TableRowColumn>
        </TableRow>
      );
    });
  }

  render() {
    const values = {
      fullName: this.state.fullName,
      iin: this.state.iin,
      phone: this.state.phone,
      contacts: this.state.contacts,
      login: this.state.login,
      password: this.state.password,
      cfullName: this.state.cfullName,
      ciin: this.state.ciin,
      cphone: this.state.cphone,
      ccontacts: this.state.ccontacts,
      conflictName: this.state.conflictName,
      preventionName: this.state.preventionName,
      region: this.state.region
    },
          snackbar = {
      message: this.state.snackbarMessage,
      open: this.state.snackbarOpen,
      closeHandler: this.closeHandler.bind(this)
    };

    return <Dictionaries
      regions={this.props.regions}
      touchTapHandler={this.touchTapHandler.bind(this)}
      changeHandler={this.changeHandler.bind(this)}
      selectChangeHandler={this.selectChangeHandler.bind(this)}
      addCollaboratorHandler={this.addCollaboratorHandler.bind(this)}
      addMediatorHandler={this.addMediatorHandler.bind(this)}
      addConflictHandler={this.addConflictHandler.bind(this)}
      addPreventionHandler={this.addPreventionHandler.bind(this)}
      clearFormHandler={this.clearFormHandler.bind(this)}
      values={values}
      snackbar={snackbar}
      collaboratorRows={this.getCollaboratorRows()}
      mediatorRows={this.getMediatorRows()}
      conflictRows={this.getConflictRows()}
      preventionRows={this.getPreventionRows()}
      context={this}
      hideCollaboratorsTab={this.props.user ? !this.props.user.profile.roles.includes('superadmin') : true}/>;
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    mediators: MediatorsCollection.find().fetch(),
    conflicts: ConflictsCollection.find().fetch(),
    preventions: PreventionsCollection.find().fetch(),
    collaborators: CollaboratorsCollection.find().fetch(),
    regions: RegionsCollection.find().fetch()
  };
}, DictionariesContainer);
