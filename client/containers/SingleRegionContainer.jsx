import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import {ReportsCollection} from '../../api/Reports.js';
import {RegionsCollection} from '../../api/Regions.js';

import SingleRegion from '../components/SingleRegion.jsx';

class SingleRegionContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      snackbarOpen: false,
      snackbarMessage: ''
    }
  }

  componentWillMount() {
    Meteor.subscribe('Reports');
    Meteor.subscribe('Regions');
  }

  snackbarCloseHandler(e) {
    this.setState({
      snackbarOpen: false
    });
  }

  render() {
    if(!this.props.location.query.id)
      window.location.href="/maps";

    const {id} = this.props.location.query,
          {regions, reports} = this.props,
          currentRegion = regions.filter((region) => region._id === id)[0],
          currentReports = reports.filter((report) => report.region === id),
          snackbar = {
            snackbarCloseHandler: this.snackbarCloseHandler.bind(this),
            message: this.state.snackbarMessage,
            open: this.state.snackbarOpen
          };

    return (
      <SingleRegion
        currentRegion={currentRegion}
        currentReports={currentReports}
        snackbar={snackbar}/>
    );
  }
}

export default createContainer(() => {
  return {
    regions: RegionsCollection.find().fetch(),
    reports: ReportsCollection.find({}, {sort: {createdAt: -1}}).fetch()
  };
}, SingleRegionContainer);
