import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Reports from '../components/Reports.jsx';
import {TableRow, TableRowColumn} from 'material-ui/Table';

import {ReportsCollection} from '../../api/Reports.js';
import {ConflictsCollection} from '../../api/Conflicts.js';

class ReportsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cabinetsNumber: '',
      phone: '',
      mediationBoard: '',
      professionalMediatorsNumber: '',
      nonProfessionalMediatorsNumber: '',
      disputesResolved: '',
      disputesResolvedTypes: '',
      meetingsNumber: '',
      byProfessionalMediatorsNum: '',
      byNonProfessionalMediatorsNum: '',
      byTrialsNum: '',
      byGovernmentBodiesNum: '',
      currentCabinetsNum: '',
      snackbarOpen: false,
      snackbarMessage: '',
      conflictsArray: []
    };
  }

  componentWillMount() {
    Meteor.subscribe('Conflicts');
    Meteor.subscribe('Reports');
  }

  closeHandler(e) {
    this.setState({
      snackbarOpen: false
    });
  }

  changeState(value, name) {
    this.setState({
      [name]: value
    });
  }

  touchTapHandler(e) {
    e.preventDefault();
  }

  changeHandler(e, newValue) {
    const stateName = e.currentTarget.getAttribute('data-state-name');

    this.setState({
      [stateName]: newValue
    });
  }

  selectChangeHandler(statename, e, index, payload) {
    if(statename === 'disputesResolvedTypes') {
      const conflictsArray = this.state.conflictsArray,
            currentConflict = this.props.conflicts.filter((item) => item._id === payload)[0];

      conflictsArray.push(currentConflict);

      this.setState({conflictsArray});
    }
    else {
      const stateName = e.target.getAttribute('data-state-name');

      this.setState({
        [statename]: payload
      });
    }
  }

  clearFormHandler(e) {
    e.preventDefault();

    this.setState({
      cabinetsNumber: '',
      phone: '',
      mediationBoard: '',
      professionalMediatorsNumber: '',
      nonProfessionalMediatorsNumber: '',
      disputesResolved: '',
      disputesResolvedTypes: '',
      meetingsNumber: '',
      byProfessionalMediatorsNum: '',
      byNonProfessionalMediatorsNum: '',
      byTrialsNum: '',
      byGovernmentBodiesNum: '',
      currentCabinetsNum: '',
      snackbarOpen: false,
      snackbarMessage: ''
    });
  }

  chipDeleteHandler(id, e) {
    e.preventDefault();

    const conflictsArray = this.state.conflictsArray,
          arrayFiltered = conflictsArray.filter((item) => item._id !== id);

    this.setState({conflictsArray: arrayFiltered});
  }

  addReportHandler(e) {
    e.preventDefault();

    const data = {
      cabinetsNumber: this.state.cabinetsNumber,
      phone: this.state.phone,
      mediationBoard: this.state.mediationBoard,
      professionalMediatorsNumber: this.state.professionalMediatorsNumber,
      nonProfessionalMediatorsNumber: this.state.nonProfessionalMediatorsNumber,
      disputesResolved: this.state.disputesResolved,
      meetingsNumber: this.state.meetingsNumber,
      byProfessionalMediatorsNum: this.state.byProfessionalMediatorsNum,
      byNonProfessionalMediatorsNum: this.state.byNonProfessionalMediatorsNum,
      byTrialsNum: this.state.byTrialsNum,
      byGovernmentBodiesNum: this.state.byGovernmentBodiesNum,
      currentCabinetsNum: this.state.currentCabinetsNum,
      conflicts: this.state.conflictsArra,
      region: this.props.user.profile.region
    };

    Meteor.call('reports.add', data, (err) => {
      if(err) {
        this.setState({
          snackbarMessage: err.reason,
          snackbarOpen: true,
        });
      }
      else {
        this.setState({
          cabinetsNumber: '',
          phone: '',
          mediationBoard: '',
          professionalMediatorsNumber: '',
          nonProfessionalMediatorsNumber: '',
          disputesResolved: '',
          disputesResolvedTypes: '',
          meetingsNumber: '',
          byProfessionalMediatorsNum: '',
          byNonProfessionalMediatorsNum: '',
          byTrialsNum: '',
          byGovernmentBodiesNum: '',
          currentCabinetsNum: '',
          snackbarOpen: false,
          snackbarMessage: '',
          conflictsArray: []
        });
      }
    });
  }

  beautify(unit)
  {
    return unit < 10 ? `0${unit}` : `${unit}`;
  }

  getReportRows() {
    return this.props.reports.map((report) => {
      const d = new Date(report.createdAt);
      return (
        <TableRow key={report._id}>
          <TableRowColumn>{report._id}</TableRowColumn>
          <TableRowColumn>{this.beautify(d.getDate())
                            + '-' + this.beautify(d.getMonth() + 1) + '-'
                            + d.getFullYear()}</TableRowColumn>
        </TableRow>
      );
    });
  }


  render() {
    const {conflicts} = this.props,
          values = {
      cabinetsNumber: this.state.cabinetsNumber,
      phone: this.state.phone,
      mediationBoard: this.state.mediationBoard,
      professionalMediatorsNumber: this.state.professionalMediatorsNumber,
      nonProfessionalMediatorsNumber: this.state.nonProfessionalMediatorsNumber,
      disputesResolved: this.state.disputesResolved,
      disputesResolvedTypes: this.state.disputesResolvedTypes,
      meetingsNumber: this.state.meetingsNumber,
      byProfessionalMediatorsNum: this.state.byProfessionalMediatorsNum,
      byNonProfessionalMediatorsNum: this.state.byNonProfessionalMediatorsNum,
      byTrialsNum: this.state.byTrialsNum,
      byGovernmentBodiesNum: this.state.byGovernmentBodiesNum,
      currentCabinetsNum: this.state.currentCabinetsNum,
      conflictsArray: this.state.conflictsArray
    },
          snackbar = {
      message: this.state.snackbarMessage,
      open: this.state.snackbarOpen,
      closeHandler: this.closeHandler.bind(this)
    };

    return <Reports
      touchTapHandler={this.touchTapHandler.bind(this)}
      changeHandler={this.changeHandler.bind(this)}
      selectChangeHandler={this.selectChangeHandler.bind(this)}
      values={values}
      snackbar={snackbar}
      context={this}
      clearFormHandler={this.clearFormHandler.bind(this)}
      conflicts={conflicts}
      reportRows={this.getReportRows()}
      chipDeleteHandler={this.chipDeleteHandler.bind(this)}
      addReportHandler={this.addReportHandler.bind(this)}/>;
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    reports: ReportsCollection.find().fetch(),
    conflicts: ConflictsCollection.find().fetch(),
  };
}, ReportsContainer);
